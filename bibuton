#!/bin/bash -e

# Configuration
# -------------

GPIO_GREEN_LED=17
GPIO_RED_LED=18
GPIO_PUSH_BUTTON=4

BLINK_DELAY=0.2
BUTTON_DELAY=0.2

source /etc/bibuton.conf

# GPIO initialization
# -------------------

gpio_init() {
	  echo $GPIO_GREEN_LED > /sys/class/gpio/export
	  echo $GPIO_RED_LED > /sys/class/gpio/export
	  echo $GPIO_PUSH_BUTTON > /sys/class/gpio/export
	  echo "out" > /sys/class/gpio/gpio${GPIO_RED_LED}/direction
	  echo "out" > /sys/class/gpio/gpio${GPIO_GREEN_LED}/direction
	  echo "in" > /sys/class/gpio/gpio${GPIO_PUSH_BUTTON}/direction
}

# LED setter functions
# --------------------

led_both() {
	  echo "1" > /sys/class/gpio/gpio${GPIO_GREEN_LED}/value
	  echo "1" > /sys/class/gpio/gpio${GPIO_RED_LED}/value
}

led_green() {
    echo "1" > /sys/class/gpio/gpio${GPIO_GREEN_LED}/value
    echo "0" > /sys/class/gpio/gpio${GPIO_RED_LED}/value
}

led_red() {
    echo "0" > /sys/class/gpio/gpio${GPIO_GREEN_LED}/value
    echo "1" > /sys/class/gpio/gpio${GPIO_RED_LED}/value
}

led_off() {
    echo "0" > /sys/class/gpio/gpio${GPIO_GREEN_LED}/value
    echo "0" > /sys/class/gpio/gpio${GPIO_RED_LED}/value
}

# LED blink function
# ------------------

led_blink_loop () {
	  while [ -f $blink_event_file ] ; do
		    led_green; sleep $BLINK_DELAY
		    led_red  ; sleep $BLINK_DELAY
	  done
}

led_blink_start () {
	  touch $blink_event_file
	  led_blink_loop &
}

led_blink_stop () {
	  rm $blink_event_file
}

# Remote I/O functions
# --------------------

remote_auth () {
    curl -k -c $cookies_file -d "name=${REMOTE_USER}&pass=${REMOTE_PASS}&form_id=user_login&op=Se+connecter" https://${REMOTE_HOST}/${REMOTE_LOGIN_URL} -s
}

fetch_state () {
    curl -k -b $cookies_file https://${REMOTE_HOST} --output $response_file -s
}

toggle_state () {
    curl -k -b $cookies_file -d "op=Changer&form_build_id=${form_build_id}&form_token=${form_token}&form_id=bibstate_toggle_form" https://${REMOTE_HOST} -s --output /dev/null
}

parse_state() {
    form_token=$(cat $response_file | grep -m 1 form_token | awk '{print $4}' | awk -F '\"' '{print $2}')
    form_build_id=$(cat $response_file | grep -m 1 form_build_id | awk '{print $29}' | awk -F '\"' '{print $2}')
    state=$(cat $response_file | grep -m 1 bibopen | awk '{print $9}' | awk -F '\"' '{print $2}')
}

# Main code
# ---------

# Initialize GPIO
gpio_init 2>/dev/null || true

# Temporary directory
tempdir=$(mktemp -dt bibuton.XXXXXX)
blink_event_file="${tempdir}/blink"
cookies_file="${tempdir}/cookies"
response_file="${tempdir}/response"

# Read initial state
echo -n "# Reading initial state : "
remote_auth
fetch_state
parse_state
[ -n "$state" ] && (echo "OPEN"; led_green) || (echo "CLOSED"; led_red)
echo "# Waiting for button press..."

# Main loop
while true; do
    if [ $(cat /sys/class/gpio/gpio${GPIO_PUSH_BUTTON}/value) -eq 0 ]; then
	      # 1. Make LEDs blink
        echo "# Button pressed !"
	      led_blink_start

	      # 2. Read remote state
	      echo -n "# Reading state... "
        remote_auth
        fetch_state
        parse_state
	      echo "done."

	      # 3. Report state to console
        echo -n "# Current state : "
        [ -n "$state" ] && echo "OPEN (closing...)" || echo "CLOSED (opening...)"

	      # 4. Toggle remote state, show with LEDs
	      echo -n "# Toggling state..."
	      toggle_state
	      led_blink_stop; sleep $BLINK_DELAY
        [ -z "$state" ] && led_green || led_red
	      echo "done."

	      echo "# Waiting for next button press..."
    else
	      sleep $BUTTON_DELAY
    fi
done
